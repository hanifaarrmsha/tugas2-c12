from django.conf.urls import url
from .views import index
from b2_profil.views import add_company
from b2_profil.views import index as profil_page

urlpatterns = [
    url(r'^$', index, name='index'),
    # url(r'^(?P<id>\d+)/$', profil_page, name='index'),
    url(r'^add_company/$',add_company,name='add_company')
]
