# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-12-13 14:00
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('b1_utama', '0004_company_forum_reply'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Company',
        ),
        migrations.RemoveField(
            model_name='reply',
            name='forum',
        ),
        migrations.DeleteModel(
            name='Forum',
        ),
        migrations.DeleteModel(
            name='Reply',
        ),
    ]
