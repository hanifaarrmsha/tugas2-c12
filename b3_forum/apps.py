from django.apps import AppConfig


class B3ForumConfig(AppConfig):
    name = 'b3_forum'
