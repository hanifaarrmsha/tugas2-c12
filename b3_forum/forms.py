from django import forms

class Forum_Lowongan(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
    }
    lowongan_attrs = {
        'type': 'text',
        'class': 'form-control',
        'placeholder':'Masukan deskripsi Forum'
    }

    lowongan = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=lowongan_attrs))
