from django.db import models
from b3_forum.models import Forum as forum

class Reply(models.Model):
    sender = models.CharField(max_length=60,default="Unknown Applicant")
    message = models.CharField(max_length=250)
    created_date = models.DateTimeField(auto_now_add=True)
    forum = models.ForeignKey(forum)
